const BME280 = require("bme280-sensor");
const express = require("express");
const client = require("prom-client");
const async = require("async");

client.register.setDefaultLabels({
  location: "office",
});

const tempGauge = new client.Gauge({
  name: "ambient_temperature",
  help: "The ambient temperature in °C as measured by the BME280 sensor",
});
const pressureGauge = new client.Gauge({
  name: "ambient_pressure",
  help: "The ambient pressure in Hectopascal as measured by the BME280 sensor",
});
const humidityGauge = new client.Gauge({
  name: "ambient_humidity",
  help: "The ambient humidity in percentage of relative humidity as measured by the BME280 sensor",
});

const app = express();
let sensorInitialised = false;

app.get("/metrics", async (req, res) => {
  if (!sensorInitialised) {
    console.warn("Sensor has not been initialised");
    res.status(425).send("Sensor has not been initialised");
    return;
  }

  try {
    const data = await bme280.readSensorData();
    tempGauge.set(data.temperature_C);
    pressureGauge.set(data.pressure_hPa);
    humidityGauge.set(data.humidity);
    const output = await client.register.metrics();

    res.type("text/plaintext").send(output);
  } catch (e) {
    console.error(e);
    res.status(500).send(e);
  }
});

const bme280 = new BME280();
let PORT = 9545;
app.listen(PORT, () => {
  console.log(`App is listening on port ${PORT}`);
});

async function connectSensor() {
  console.log("Initialising sensor");
  try {
    await bme280.init();
    sensorInitialised = true;
  } catch (e) {
    console.error("Failed to connect to sensor.");
    console.error(e);
    throw e; // Re-throw so retry handler can kick in.
  }
}

function initSensor(retryOpts) {
  return async.retry(retryOpts, connectSensor);
}

initSensor({ times: 50, interval: 500 }).catch(() => {
  console.error("Failed to initialise sensor after 5 tries");
  process.exit(1);
});
